# Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [5.2.1] - 16-05-2019

### Added
* Error message now printes when the attribute ErrMessage is written by Matlab (to ease debugging)
* 
## [5.2.0] - 26-03-2019

### Added
* New property (MatlabRingName) for the Matlab script
## [5.1.0] - 15-01-2019

### Changed
* Increase the max number of open files to 2048 (in main.cpp)
in order to support the large number of connection
during a "reset_ebs_simu" Matlab command
(> 1000 connections)

## [5.0.0] - 08-11-2018

### Added
* Add three attributes:
    * Radio frequency voltage (named RfVoltage)
    * Jitter on quadrupole gradient (named QuadGradJit)
    * Quadrupole vibration amplitude (named QuadVibAmpl)

## [4.0.0] - 17-10-2018

### Added
* Add a BPM noise attribute (Named BpmNoise)

## [3.0.0] - 26-09-2018

### Changed

* Flip X and Y data for attributes HPositionsTbT and VPositionsTbT

## [2.0.1] - 28-08-2018

### Changed
* Matlab 2018a is now the default at ESRF. This matlab release requires C++ std lib 3.4.21. On debian8, we have only 3.4.20! Take C++ std lib from a debian 9 host. **Warning:** If this DS is runing on a debian 8 host, it requires a special setup to find the required C++ std lib.

## [2.0.0] - 31-05-2018

### Added
* ESRF's README and CHANGELOG official files
