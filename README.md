# RingSimulator

Tango class which is doing the interface between the matlab EBS simulation and the Tango world. This class is at the heart of the
EBS simulator

## Cloning 

To clone this project, type

```
git clone git@gitlab.esrf.fr:accelerator/Simulators/EbsSimulator/RingSimulator.git
```

## Documentation

Pogo generated doc in **doc_html** folder

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies

* Matlab: Today (08/2018), compiled and run with Matlab release 2018a which since summer 2018 is the ESRF default. This matlab release requires a specific C++ std lib release (6.0.22) which unfortunately is not available on ESRF debian 8 installation. Take it from debian 9!
* C++ std lib 6.0.22 (GLIBC 3.4.21)
* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.

#### Toolchain Dependencies

* C++11 compliant compiler.

### Build

Instructions on building the project.

Make example:

```bash
cd RingSimulator
make
```

### Installation

This device server has to be installed in a host with the same Matlab release than
the Matlab release used during the compilation process.
